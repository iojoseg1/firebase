package com.example.firebase2

import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.example.firebase2.databinding.ActivityMainBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)



        //Read a messages to the database
        val database = Firebase.database.reference

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()){
                val data = snapshot.getValue(String::class.java)
                    binding.TvFirebase.text = "Firebase Remote: $data"
            }else{
                binding.TvFirebase.text = "Ruta sin datos"
             }
            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@MainActivity,"Error al leer datos", Toast.LENGTH_LONG).show()
            }
        }
        val dataRef = database.child("Ejemplo_firebase").child("data")
        dataRef.addValueEventListener(listener)

        // Evento del Boton
        binding.btnSend.setOnClickListener {
            val data = binding.etData.text.toString()
            dataRef.setValue(data)
            binding.etData.text!!.clear()
        }

        binding.btnSend.setOnLongClickListener {
            dataRef.removeValue()
            true
        }
    }

}